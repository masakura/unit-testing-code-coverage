using Xunit;

namespace XUnit.Coverlet
{
    public sealed class FailedTest
    {
        [Fact(Skip = "テストを強制的に失敗させる")]
        public void 失敗()
        {
            Assert.True(false, "必ず失敗する");
        }
    }
}
