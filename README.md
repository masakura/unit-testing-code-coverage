# .NET Unit Test Code Coverage + GitLab
[単体テストにコードカバレッジを使用する](https://docs.microsoft.com/ja-jp/dotnet/core/testing/unit-testing-code-coverage)のコードカバレッジを GitLab で表示するサンプル。

GitLab の Merge Request からコードカバレッジを見る方法は三種類あります。[デモ用の Merge Request](!9) で確認できます。

| 種類 | 手順 | スクリーンショット
| --- | --- | ---
| GitLab Artifacts 経由でのフルのカバレッジレポート | `View exposed artifacts` -> `coverage` -> `coverage` -> `index.html` | ![](./docs/html-coverage.png) 
| 総カバレッジ率 | | ![](./docs/coverage-percent-merge-request.png)
| コードカバレッジ | `Changes` のあとページリロード | ![](./docs/coverage-changes.png)


## カバレッジのとり方
次のコマンドで単体テストの実行ができます。

```
$ dotnet test
```

カバレッジを取るには次のコマンドになります。

```
$ dotnet test --collect:"XPlat Code Coverage"

...

添付ファイル:
  /.../XUnit.Coverlet.Collector/TestResults/c9fcdd9e-929f-4c53-a918-9daa9bc4094e/coverage.cobertura.xml
```

プロジェクトフォルダーの `TestResults/.../coverage.cobertura.xml` にカバレッジレポートが出力されます。

```xml
<?xml version="1.0" encoding="utf-8"?>
<coverage line-rate="1" branch-rate="1" version="1.9" timestamp="1629691837" lines-covered="12" lines-valid="12" branches-covered="6" branches-valid="6">
  <sources>
    <source>/</source>
  </sources>
  <packages>
    <package name="Numbers" line-rate="1" branch-rate="1" complexity="6">
      <classes>
        <class name="System.Numbers.PrimeService" filename="home/masakura/RiderProjects/unit-testing-code-coverage/Numbers/PrimeService.cs" line-rate="1" branch-rate="1" complexity="6">
          <methods>
            <method name="IsPrime" signature="(System.Int32)" line-rate="1" branch-rate="1" complexity="6">
            <method name="IsPrime" signature="(System.Int32)" line-rate="1" branch-rate="1" complexity="6">
              <lines>
                <line number="6" hits="11" branch="False" />
                <line number="7" hits="11" branch="True" condition-coverage="100% (2/2)">
                  <conditions>
                    <condition number="7" type="jump" coverage="100%" />
                  </conditions>
                </line>
<!-- ... -->
```


## GitLab との組み合わせ
### カバレッジレポートを HTML 化して表示する
GitLab CI で作成したカバレッジレポートの XML を GitLab Artifacts にアップロードしておくと、GitLab CI ジョブからダウンロードして見ることができます。

`.gitlab-ci.yml` を次のように書いておくと、GitLab Artifacts 経由でカバレッジレポートをダウンロードして見ることができます。

```yaml
image: mcr.microsoft.com/dotnet/sdk:5.0

test:
  script:
    - dotnet test --collect:"XPlat Code Coverage"
  artifacts:
    paths:
      - './**/coverage.cobertura.xml'
```

ただし、問題が多いです。

* XML の生のデータを見てもよくわからない
* Merge Request から該当の GitLab CI ジョブを開いて、ダウンロードして、解凍して... と手順が多い

XML のカバレッジレポートを見やすいように HTML 化します。[レポートの生成](https://docs.microsoft.com/ja-jp/dotnet/core/testing/unit-testing-code-coverage#generate-reports)にあるように、[ReportGenerator](https://github.com/danielpalme/ReportGenerator) を利用して、HTML のカバレッジレポートを生成します。

```yaml
build:
  script:
    - dotnet test --collect:"XPlat Code Coverage"
    - dotnet reportgenerator -reports:./*/TestResults/*/coverage.cobertura.xml -targetdir:./coverage
  artifacts:
    paths: [coverage/]
```

Merge Request のパイプラインから `test` ジョブを選んで、`Browse` -> `coverage` -> `index.html` と選ぶと次のように見やすいカバレッジレポートを見ることができます。

![](./docs/html-coverage.png)

しかし、カバレッジレポートを見るための手順が多く、手順を知らないと見ることができません。

Merge Request のページに GitLab Artifacts のリンクを貼ることができるので、それを使うとちょっとだけ緩和できます。

```yaml
build:
  script:
    - dotnet test --collect:"XPlat Code Coverage"
    - dotnet reportgenerator -reports:./*/TestResults/*/coverage.cobertura.xml -targetdir:./coverage
  artifacts:
    expose_as: coverage
    paths: [coverage/]
```

次のように Merge Request のページの `View exposed artifacts` をクリックすると、GitLab Artifacts へのリンクが表示されるようになります。

![](./docs/expose-as-link.png)

ただし、このリンクは `coverage/index.hthml` へのリンクではないため、coverage -> index.html とクリックする必要はあります。また、カバレッジレポートをこの方法で見ることができることを開発者が知っている必要があります。

この方法はレポートの自由度が高く大変有用ですが、開発者がカバレッジレポートに気が付きにくい問題があります。


### Merge Request に総カバレッジ率を表示する
GitLab CI ジョブに表示されている総カバレッジ率を Merge Request などに表示することができます。

| 場所 | スクリーンショット
| --- | ---
| Merge Request | ![](./docs/coverage-percent-merge-request.png)
| GitLab CI ジョブ一覧 | ![](./docs/coverage-percent-job-list.png)
| GitLab CI ジョブの右上 | ![](./docs/coverage-percent-job.png)
| Repository Analytics | ![](./docs/coverage-statistics.png)

この方法は、カバレッジ率を見るための手順が簡単でわかりやすいことが利点です。反面、総カバレッジ率しか表示されないので、これをもとに品質の判断をするのが難しい。


### Merge Request の Changes のコード差分にコードカバレッジを表示する
Merge Request の Changes にあるコード差分にカバレッジを表示することができます。

![](./docs/coverage-changes.png)

GitLab Artifacts の `cobertura` レポートに、Cobertura 形式のレポートを指定するだけです。幸い、今回作成された XML 形式のカバレッジレポートは Cobertura 形式です。

```yaml
build:
  script:
    - dotnet test --collect:"XPlat Code Coverage"
  artifacts:
    reports:
      cobertura: ./**/coverage.cobertura.xml
```

この方法は、Merge Request のコードレビューで、テストが不足しているところを発見するのに役に立ちそうです。その代わり、カバレッジ率を俯瞰で見ることはできないようです。

なお、`dotnet test ...` で生成される Cobertura 形式のカバレッジレポートですが、コードファイルへのパスが GitLab の要求する形式に合わないため、レポートファイルを変換する必要があります。

GitLab 次のように、`/builds/{namespace}/{project}` ディレクトリからの相対パスが設定されていることを期待するようです。

```xml
<sources>
    <source>/builds/{namespace}/{project}</source>
</sources>

<!-- ... -->
<class name="System.Numbers.PrimeService" filename="Numbers/PrimeService.cs" line-rate="1" branch-rate="1" complexity="6">
```

残念ながら `dotnet test ...` で生成される Cobertura 形式のカバレッジレポートは `/` からの相対パスになっています。

```xml
<sources>
    <source>/</source>
</sources>

<!-- ... -->
<class name="System.Numbers.PrimeService" filename="builds/{namespace}/{project}/Numbers/PrimeService.cs" line-rate="1" branch-rate="1" complexity="6">
```

このため、レポートを変換する必要があります。どのように変換するかは !5 を見てください。

軽く調べた範囲では、`dotnet test ...` で基準パスを変更することは難しいようです。


### その他
他、このサンプルプロジェクトでは次のことも含んでいます。

* GitLab CI の単体テストジョブが失敗しても、カバレッジレポートを見られるように
* 単体テストレポートを Merge Request などで見られるように

